extends RigidBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_description() -> String:
	return name + ":Container"

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func processSelectedObj(delta):
	pass

func capturePossible():
	return true
	
func _on_ContainerDry40__input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			owner.set_selectedObj(self)
