extends RigidBody

var setHAimant:float = 0.0
	# consigne de hauteur 

var pickedObj:Node = null

# Called when the node enters the scene tree for the first time.
func _ready():
	setHAimant = $StaticBodyAimant.transform.origin.y
	pass # Replace with function body.

func get_description() -> String:
	return name + ":Crane"
	
func processSelectedObj(delta):
	if Input.is_key_pressed(KEY_Z):
		add_central_force(get_transform().basis.xform(Vector3(10.0, 0.0, 0.0)))
	if Input.is_key_pressed(KEY_S):
		add_central_force(get_transform().basis.xform(Vector3(-10.0, 0.0, 0.0)))
	if Input.is_key_pressed(KEY_R): setHAimant += delta * 2.0
	if Input.is_key_pressed(KEY_F): setHAimant -= delta * 2.0
	if Input.is_key_pressed(KEY_W) and pickedObj==null:
		var lObj = $StaticBodyAimant/AreaPrise.get_overlapping_bodies()
		if lObj.size() > 0:
			#pickObject(lObj[0])
			call_deferred("pickObject",lObj[0])
	if Input.is_key_pressed(KEY_X) and pickedObj!=null:
		#unpickObject()
		call_deferred("unpickObject")

func manage_aimant(delta):
	if abs(setHAimant-$StaticBodyAimant.transform.origin.y)>0.05:
		var depl:float = setHAimant - $StaticBodyAimant.transform.origin.y
		# depl = min(delta,max(-delta,depl))
		$StaticBodyAimant.translate_object_local(Vector3(0.0,depl,0.0))	

func manage_pickedObj(delta):
	if pickedObj != null:
		pass
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	manage_aimant(delta)
	manage_pickedObj(delta)

func pickObject(obj:Node):
	pickedObj = obj
	print ("(Gantry) pickObject de ",get_path_to(obj),",",typeof(obj))
	var pos = obj.global_transform
	owner.remove_child(obj)
	obj.set_mode(1) # static
	obj.set_use_custom_integrator(true) # pas de memorisation des forces
	#$CollisionShape2.add_child_below_node(obj,$CollisionShape2/MeshInstance2)
	#$CollisionShape2.add_child(obj)
	$StaticBodyAimant.add_child(obj)
	obj.set_global_transform(pos)
	pass

func unpickObject():
	print ("(Gantry) unpickObject de ",get_path_to(pickedObj),",",typeof(pickedObj))
	var pos = pickedObj.global_transform
	#$CollisionShape2.remove_child(pickedObj)
	$StaticBodyAimant.remove_child(pickedObj)
	#remove_child(pickedObj)
	pickedObj.set_global_transform(pos)
	pickedObj.set_use_custom_integrator(false)
	#for e in pickedObj.get_children():
	#	if e is CollisionShape: 
	#		e.disabled = false
	pickedObj.set_mode(0) # MODE_RIGID
	owner.add_child(pickedObj)
	pickedObj.add_central_force(Vector3(0.0,0.0,0))
	pickedObj = null
	pass

func _on_GantryAGV_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			owner.set_selectedObj(self)

func _on_Area_body_entered(body):
	print ("(Gantry) Arrive de ",get_path_to(body),",",typeof(body))


func _on_Area_body_exited(body):
	print ("(Gantry) Depart de ",body.get_path())
	print ("(Gantry) Depart de ",get_path_to(body))
