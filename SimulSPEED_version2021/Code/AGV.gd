extends VehicleBody

var parent:Node = null
var dir:float = 0
func set_parent(obj:Node):
	parent = obj

#var posCamera:Vector3
#var orientCamera:Vector3

func _ready():
	#posCamera = $ViewportContainer/Viewport/CameraImg.get_transform().origin
	#orientCamera = $ViewportContainer/Viewport/CameraImg.rotation
	#print ("Orientation:",orientCamera)
	pass # Replace with function body.

func getComm_speed() -> String:
	var vit:float = linear_velocity.length()
	return String(vit)

func getComm_position() -> String:
	return (String(get_global_transform().origin.x) + ","
		+ String(get_global_transform().origin.z) + ","
		+ String(get_global_transform().basis.get_euler()[1]))
		
func getComm_camera() -> String:
	#var vit:float = linear_velocity.length()
	parent.array_reponse = getImage().save_png_to_buffer()
	return "$ARRAY"
	
func setComm_steering(value):
	dir = value
	steering = value

func setComm_engine(value):
	engine_force = value
	getComm_speed()

func processSelectedObj(delta):
	var force:float = 0.0
	if Input.is_key_pressed(KEY_Z): force = 200.0
	if Input.is_key_pressed(KEY_S): force = -200.0
	engine_force = force
	dir = 0.0
	if Input.is_key_pressed(KEY_D): dir = -1.0
	if Input.is_key_pressed(KEY_Q): dir = 1.0
	if Input.is_key_pressed(KEY_SPACE): # inutile
		dir = 0.0	
		engine_force = 0
	if steering > dir : 
		steering = steering - delta
	if steering < dir : 
		steering = steering + delta

func get_description() -> String:
	return name + ":" + "AGV20f"

func _process(delta):
	test_placeCamera()
	pass

func _on_UglyCar_input_event(camera, event, click_position, click_normal, shape_idx):
	if parent != null and event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			print ("car Selected")
			parent.set_selectedObj(self)

func getImage():
	#$ViewportContainer/Viewport/CameraImg.global_transform=global_transform
	#$ViewportContainer/Viewport/CameraImg.translate_object_local(posCamera)
	#$ViewportContainer/Viewport/CameraImg.rotate_y(-90)
	var img = $Viewport.get_texture().get_data()
	#var img = $ViewportContainer/Viewport.get_texture().get_data()
	#printt ("Height=",img.get_height(),",width=",img.get_width(),"Format=",img.get_format())
	#img.flip_y()
	return img
	
func test_lectPositionCamera():
	#return $Camera.global_transform.origin
	return $ViewportContainer/Viewport/CameraImg.global_transform.origin

func test_affCamera():
	$ViewportContainer.visible = true

func test_placeCamera():
	#$ViewportContainer/Viewport/CameraImg.global_transform=$Camera.global_transform
	$Viewport/CameraImg.global_transform=$Camera.global_transform
	#$ViewportContainer/Viewport/Camera.translate_object_local(posCamera)
	#$ViewportContainer/Viewport/Camera.rotate_y(-90)
