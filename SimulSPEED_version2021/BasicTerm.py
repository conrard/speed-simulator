import socket

UDP_IP = "127.0.0.1"
UDP_PORT = 1234

print ("UDP target IP:", UDP_IP)
print ("UDP target port:", UDP_PORT)

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM)
while (True):
    msg = input("Msg to send (empty=quit):")
    if msg=="": break
    sock.sendto(bytes(msg,"ascii"), (UDP_IP, UDP_PORT))
    print ("Msg sent")
    print ("Wait for a reply")
    data, addr = sock.recvfrom(1024)
    print ("valeur reçue:", data)
socket.close(socket.fileno())
