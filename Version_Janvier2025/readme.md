# Globalement
- Version opérationnelle
- utilisée comme démonstrateur lors du steering committee Safari de Séville (fev. 2025)

# SimulPort_final_Jan25
- opérationnel, nécessitant : Godot_v3.6-stable_win64.exe
- réalisé sur la base du PFE 2020/21
- communication UDP redéveloppée pour :
	- commandes optimisées en texte
	- plusieurs clients simultanées

# IHM_python
- ensemble de codes python permettant le pilotage d'équipement
- `IHM_ctrl_AGV_version2025.py` : pilotage d'AGV
- `IHM_ctrl_crane_version2025.py` : pilotage de (grande) grue
- `IHM_ctrl_gantry_crane_version2025.py` : pilotage de portique mobile
- `IHM_ctrl_multi_version2025.py` : pilotage de tous les équipements en une unique interface
- `modeAutomatique.py` : déchargement d'un unique conteneur par le grue 1, transporté par l'AGV 1 et stocké par le portique 1. Parfois (25%), le conteneur n'est pas saisi et la mission échoue.
- `modeAutomatique_thread.py` : idem à `modeAutomatique.py`, mais avec un thread permettant un mouvement parallèle de la grue et de l'AGV
- `consol_SimulPort.py` : un terminal basique pour tester la communication